﻿CREATE PROCEDURE [dbo].[uspgetuser]
	@Command Varchar(100)=NULL,

	@Userid Numeric(18,0)


AS
	
		IF @Command='UserData'
			BEGIN
				SELECT 
				   --  (select
					  --u.firstname + ' ' + u.lastname AS FullName
					  -- from tbluser as u),
					u.firstname + ' ' + u.lastname AS fullname,
					u.userid,
					u.firstname,
					u.lastname,
					u.age,
					u.dob,
					u.gender,
					c.cityid,
					c.cityname
					FROM tbluser AS u
						INNER JOIN 
							tblcity AS c
								ON
									u.cityid=c.cityid
						WHERE u.userid=@Userid
			END
		ELSE IF @Command='UserHobbyData'
			BEGIN
				SELECT 
					h.hobbyid,
					h.hobbyname,
					h.interested
					FROM tbluserhobby AS h
						WHERE h.userid=@Userid
			END