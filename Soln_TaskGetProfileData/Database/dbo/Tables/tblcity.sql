﻿CREATE TABLE [dbo].[tblcity] (
    [cityid]   NVARCHAR (9)   NOT NULL,
    [cityname] NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_tblcity] PRIMARY KEY CLUSTERED ([cityid] ASC)
);

