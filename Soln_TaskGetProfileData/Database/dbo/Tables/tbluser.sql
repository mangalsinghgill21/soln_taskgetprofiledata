﻿CREATE TABLE [dbo].[tbluser] (
    [userid]    NUMERIC (18)   IDENTITY (1, 1) NOT NULL,
    [firstname] NVARCHAR (MAX) NOT NULL,
    [lastname]  NVARCHAR (MAX) NOT NULL,
    [age]       NUMERIC (18)   NOT NULL,
    [dob]       DATE           NOT NULL,
    [gender]    BIT            NOT NULL,
    [cityid]    NUMERIC (18)   NOT NULL,
    CONSTRAINT [PK_tbluser] PRIMARY KEY CLUSTERED ([userid] ASC)
);

