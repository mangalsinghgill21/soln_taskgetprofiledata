﻿CREATE TABLE [dbo].[tbluserhobby] (
    [hobbyid]    NUMERIC (18)   IDENTITY (1, 1) NOT NULL,
    [hobbyname]  NVARCHAR (MAX) NOT NULL,
    [interested] BIT            NOT NULL,
    [userid]     NUMERIC (18)   NOT NULL,
    CONSTRAINT [PK_tbluserhobby] PRIMARY KEY CLUSTERED ([hobbyid] ASC)
);

