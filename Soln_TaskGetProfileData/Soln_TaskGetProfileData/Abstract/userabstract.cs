﻿using Soln_TaskGetProfileData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Soln_TaskGetProfileData.Abstract
{
    public abstract class userabstract : IDisposable
    {
        public void Dispose();
        
        public abstract Task<IEnumerable<cityentity>> GetCityData();
    }
}