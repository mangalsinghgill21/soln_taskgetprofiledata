﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Soln_TaskGetProfileData.Models
{
    public class userhobbyentity
    {
        public dynamic hobbyid { get; set; }

        public dynamic hobbyname { get; set; }

        public dynamic interested { get; set; }

        public dynamic userid { get; set; }
    }
}