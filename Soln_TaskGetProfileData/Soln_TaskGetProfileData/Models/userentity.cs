﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Soln_TaskGetProfileData.Models
{
    public class userentity
    {
        public dynamic userid { get; set; }
        public dynamic firstname { get; set; }
        public dynamic lastname { get; set; }
        public dynamic fullname { get; set; }
        public dynamic age { get; set; }
        public dynamic gender { get; set; }
        public dynamic datebirth { get; set; }
        public dynamic cityid { get; set; }
        public List<userhobbyentity> userhobby { get; set; }
        
    }
}